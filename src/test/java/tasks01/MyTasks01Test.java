package tasks01;

import com.novarto.training22.tasks01.MyTasks01;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MyTasks01Test {
    MyTasks01 myTasks01 = new MyTasks01();
    //Task 1
    @Test
    public void givenTwoIntegersWhenTheyAreEqualThenReturnTheNumberWithoutException() {
        double actualOutput = myTasks01.returnBiggerValue(45, 45);
        assertEquals(45, actualOutput);
    }

    @Test
    public void givenTwoIntegersWhenTheyAreNegativeThenReturnTheBiggerOne() {
        double actualOutput = myTasks01.returnBiggerValue(-1000, -450);
        assertEquals(-450, actualOutput);
    }

    //Task 2
    @Test()
    public void givenThreeValuesWhenDividerIsZeroThenMethodShouldThrowException() {
        Exception exception = assertThrows(ArithmeticException.class, () -> {
            myTasks01.calculateX(0, 5, 7);
        });

        String expectedMessage = "Divided by zero operation is not possible";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void givenThreeValuesWhenTheyAreNegativeNumbersThenMethodShouldCalculateX() {
        double actualOutput = myTasks01.calculateX(-2, -3, -5);
        assertEquals(1.0, actualOutput);
    }

    //Task 3a
    @Test
    public void givenTwoValuesToCalculateTriangleAreaWhenOneOfThemIsNegativeThenMethodShouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks01.calculateRightTriangleArea(-5, 7);
        });

        String expectedMessage = "The triangle cannot have sides with negative values or zero.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void givenTwoValuesToCalculateTriangleAreaWhenOneOfThemIsZeroThenMethodShouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks01.calculateRightTriangleArea(0, 5);
        });

        String expectedMessage = "The triangle cannot have sides with negative values or zero.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void givenTwoValuesToCalculateTriangleAreaWhenAllValuesArePositiveThenMethodShouldCalculateArea() {
        double actualOutput = myTasks01.calculateRightTriangleArea(5, 4);
        assertEquals(10.0, actualOutput);
    }

    //Task 3b
    @Test
    public void givenTwoValuesToCalculateTrianglePerimeterWhenOneOfThemIsNegativeThenMethodShouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks01.calculateRightTrianglePerimeter(-5, 7);
        });

        String expectedMessage = "The triangle cannot have sides with negative values or zero.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void givenTwoValuesToCalculateTrianglePerimeterWhenOneOfThemIsZeroThenMethodShouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks01.calculateRightTrianglePerimeter(0, 7);
        });

        String expectedMessage = "The triangle cannot have sides with negative values or zero.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void givenTwoValuesToCalculateTrianglePerimeterWhenAllValuesArePositiveThenMethodShouldCalculateIt() {
        double actualOutput = myTasks01.calculateRightTrianglePerimeter(5, 7);
        assertEquals(20.602325267042627, actualOutput);
    }
}