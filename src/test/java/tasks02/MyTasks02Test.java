package tasks02;

import com.novarto.training22.tasks02.MyTasks02;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MyTasks02Test {
    MyTasks02 myTasks02 = new MyTasks02();
    List<Integer> positiveNumbers = List.of(
            199, 200, 208, 210, 200,
            207, 240, 269, 260, 263);

    List<Integer> negativeNumbers = List.of(
            -239, -238, -240, -237, -236,
            -235, -250, -251, -246);

    //Task01a
    @Test
    public void givenSequenceOfNumbersWhenAllNumbersArePositiveIntegersThenMethodReturnCountOfAllBiggerNumbers() {
        int actualOutput = myTasks02.countNextBiggerNumber(positiveNumbers);

        assertEquals(7, actualOutput);
    }

    //Task01a
    @Test
   public void givenSequenceOfNumbersWhenSomeNumbersAreNegativeIntegersThenMethodShouldReturnTheCorrectCountOfBiggerNumbers() {
        int actualOutput = myTasks02.countNextBiggerNumber(negativeNumbers);
        assertEquals(5, actualOutput);
    }

    @Test
    public void givenSequenceOfNumbersWhenTheNumbersAreExactlyTwoThenMethodShouldReturnOneIfTheSecondNumberIsBigger() {
        List<Integer> nums = List.of(45, 159);

        assertEquals(1, myTasks02.countNextBiggerNumber(nums));
    }

    @Test
    public void givenSequenceOfNumbersWhenTheNumbersAreExactlyTwoThenMethodShouldReturnZeroIfTheFirstOneIsBigger() {
        List<Integer> nums = List.of(160, 159);

        assertEquals(0, myTasks02.countNextBiggerNumber(nums));
    }

    //Task01b
    @Test
    public void givenSequenceOfNumbersWhenAllNumbersArePositiveThenReturnCountOfAllNextBiggerSumsOfThreeNumbers() {
        int actualOutput = myTasks02.countBiggerSums(positiveNumbers);
        assertEquals(7, actualOutput);
    }

    //Task01b
    @Test
    public void givenSequenceOfNumbersWhenSomeNumbersAreNegativeThenReturnCountOfAllNextBiggerSumsOfThreeNumbers() {
        int actualOutput = myTasks02.countBiggerSums(negativeNumbers);
        assertEquals(3, actualOutput);
    }

    //Task1b
    @Test
    public void givenSequenceOfNumbersWhenSizeOfTheListOfNumbersIsLessThanTwoThenMethodShouldThrowException() {
        List<Integer> nums = List.of(256);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks02.countBiggerSums(nums);
        });

        assertEquals("No comparison possible with one or less than one number.", exception.getMessage());
    }

    @Test
    public void givenSequenceOfNumbersWhenTheNumbersListIsWithSizeTwoThenReturnOne() {
        List<Integer> nums = List.of(199, 230);

        assertEquals(1, myTasks02.countBiggerSums(nums));
    }

    //Task02
    @Test
    public void givenStringThatContainsCommandAndCoordinatesWhenAllCommandsAreCorrectThenMethodShouldFindTheSubmarinesCurrentCoordinates() {
        String[] coordinates = {
                "forward 2", "down 5", "forward 5", "up 3", "forward 8", "down 8"
        };
        int actualOutput = myTasks02.findSubmarine(coordinates);
        assertEquals(150, actualOutput);
    }

    //Task02
    @Test
    public void givenStringThatContainsCommandAndCoordinatesWhenCommandIsNotCorrectThenMethodShouldThrowException() {
        String[] coordinates = {
                "backward 2", "down 5", "forward 5",
                "up 3", "forward 8", "down 8"
        };

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks02.findSubmarine(coordinates);
        });

        assertEquals("Invalid command! The right commands are: up, down, forward", exception.getMessage());
    }

    //Task02
    @Test
    public void givenStringThatShouldContainsCommandAndCoordinatesWhenCoordinatesDoesNotExistThenExceptionIsThrown() {
        String[] coordinates = {
                "forward3", "down 5", "forward 5",
                "up 3", "forward 8", "down 8"
        };

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks02.findSubmarine(coordinates);
        });

        assertEquals("Invalid coordinates!", exception.getMessage());
    }

    //Task02
    @Test
    public void givenCorrectCommandWhenDirectionIsNegativeValueThenExceptionIsThrown() {
        String[] coordinate = {
                "forward 10", "down 5", "up -15"
        };

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            myTasks02.findSubmarine(coordinate);
        });

        assertEquals("Invalid coordinates!", exception.getMessage());
    }

    //Task02
    @Test
    public void givenAllCorrectCommandsWhenEndDepthIsNegativeNumberThenMethodShouldReturnZero() {
        String[] coordinate = {
                "forward 10", "down 5", "up 10"
        };

        assertEquals(0, myTasks02.findSubmarine(coordinate));
    }
}