package tasks02.schoolNotebook;

import com.novarto.training22.tasks02.schoolNotebook.StudentNotebook;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.Map;


public class StudentNotebookTest {
    StudentNotebook studentNotebook = new StudentNotebook();

    @Test
    void givenSubjectAndSchoolNotesWhenItIsNotSaveThenSaveIt() {
        studentNotebook.saveSchoolGrades("Math", 5.25);
        studentNotebook.saveSchoolGrades("English", 3.00);

        assertTrue(studentNotebook.getAllGrades().containsKey("Math"));
        assertTrue(studentNotebook.getAllGrades().containsKey("English"));
        assertEquals(5.25, studentNotebook.getAllGrades().get("Math").get(0));
        assertEquals(3.00, studentNotebook.getAllGrades().get("English").get(0));
    }

    @Test
    void givenSubjectAndSchoolNotesWhenSubjectIsSavedThenSaveOnlySchoolNoteToIt() {
        studentNotebook.saveSchoolGrades("Math", 5.25);
        studentNotebook.saveSchoolGrades("Math", 3.00);
        studentNotebook.saveSchoolGrades("Math", 3.00);

        studentNotebook.saveSchoolGrades("Biology", 3.00);

        studentNotebook.saveSchoolGrades("Music", 5.50);
        studentNotebook.saveSchoolGrades("Music", 5.00);

        studentNotebook.saveSchoolGrades("English", 6.00);

        Map<String, List<Double>> notes = studentNotebook.getAllGrades();

        assertEquals(4, notes.size());
        assertEquals(3, notes.get("Math").size());
        assertTrue(notes.get("Math").contains(3.00));
        assertTrue(notes.get("Music").contains(5.00));
    }

    @Test
    public void givenSubjectAndSchoolNoteWhenSchoolNoteIsLessThanTwoThenShouldThrowAnException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            studentNotebook.saveSchoolGrades("Math", 1.50);
        });

        assertEquals("School notes can't be less then 2 and more then 6!", exception.getMessage());
    }

    @Test
    public void givenSubjectAndSchoolNoteWhenSchoolNoteIsMoreThanSixThenShouldThrowAnException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            studentNotebook.saveSchoolGrades("Math", 7.00);
        });

        assertEquals("School notes can't be less then 2 and more then 6!", exception.getMessage());
    }

    @Test
    public void givenSubjectWhenItIsNotPresentThenMethodShouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            studentNotebook.saveSchoolGrades("English", 5.00);
            studentNotebook.saveSchoolGrades("Biology", 4.00);
            studentNotebook.getAverageGrade("Math");
        });

        assertEquals("This subject doesn't exist in the database :(", exception.getMessage());
    }

    @Test
    public void givenSubjectWhenMapISEmptyThenMethodShouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            studentNotebook.getAverageGrade("Math");
        });

        assertEquals("This subject doesn't exist in the database :(", exception.getMessage());
    }

    @Test
    public void givenSubjectWhenItIsPresentThenMethodShouldReturnTheAverageSchoolNoteForIt() {
        studentNotebook.saveSchoolGrades("Math", 5.15);
        studentNotebook.saveSchoolGrades("Math", 4.50);
        studentNotebook.saveSchoolGrades("Math", 6.00);

        assertEquals(5.22, studentNotebook.getAverageGrade("Math"));
    }
    @Test
    public void givenAllSubjectsAndNotesWhenMapIsNotEmptyThenShouldReturnTheAverageNoteForAllSubjects() {
        studentNotebook.saveSchoolGrades("Math", 6.00);
        studentNotebook.saveSchoolGrades("Math", 4.56);
        studentNotebook.saveSchoolGrades("Math", 5.35);

        studentNotebook.saveSchoolGrades("English", 6.00);
        studentNotebook.saveSchoolGrades("English", 5.65);
        studentNotebook.saveSchoolGrades("English", 4.00);

        studentNotebook.saveSchoolGrades("Biology", 5.45);
        studentNotebook.saveSchoolGrades("Biology", 4.25);
        studentNotebook.saveSchoolGrades("Biology", 6.00);

        assertEquals(5.25, studentNotebook.getAllSubjectsAverageGrade(studentNotebook.getAllGrades()));
    }
    @Test
    public void givenAllSubjectsAndNotesWhenMapIsEmptyThenShouldReturnException() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
           studentNotebook.getAllSubjectsAverageGrade(studentNotebook.getAllGrades());
        });

        assertEquals("No saved data in the database!", exception.getMessage());
    }

}