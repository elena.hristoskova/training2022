package com.novarto.training22.tasks01;

public class MyTasks01 {
    //Task 1
    public int returnBiggerValue(int num1, int num2) {
        return Math.max(num1, num2);
    }
    //Task 2
    public double calculateX(double a, double b, double c) {
            if (a == 0) {
                throw new ArithmeticException("Divided by zero operation is not possible");
            } else {
                return (c - b) / a;
            }
    }
    //Task 3a
    public double calculateRightTriangleArea(double a, double b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException("The triangle cannot have sides with negative values or zero.");
        }

        return (a * b) / 2;
    }
    //Task 3b
    public double calculateRightTrianglePerimeter(double a, double b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException("" +
                    "The triangle cannot have sides with negative values or zero.");
        }

        double c = Math.pow(a, 2) + Math.pow(b, 2);
        return a + b + (Math.sqrt(c));
    }
}