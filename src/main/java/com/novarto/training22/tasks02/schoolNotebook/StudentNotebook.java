package com.novarto.training22.tasks02.schoolNotebook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentNotebook implements Notebook {
    private Map<String, List<Double>> allGrades = new HashMap<>();

    public Map<String, List<Double>> getAllGrades() {
        return allGrades;
    }

    public void setAllGrades(Map<String, List<Double>> allGrades) {
        this.allGrades = allGrades;
    }

    @Override
    public void saveSchoolGrades(String subject, double grade) {
        if (grade < 2 || grade > 6) {
            throw new IllegalArgumentException("School notes can't be less then 2 and more then 6!");
        }

        if (!this.getAllGrades().containsKey(subject)) {
            this.getAllGrades().put(subject, new ArrayList<>());
        }
        this.getAllGrades().get(subject).add(grade);
    }

    @Override
    public double getAverageGrade(String subject) {
        boolean isAvailable = this.getAllGrades().containsKey(subject);

        if (!isAvailable) {
            throw new IllegalArgumentException("This subject doesn't exist in the database :(");
        }

        double allGradesSum = this.getAllGrades().get(subject)
                .stream()
                .mapToDouble(subjectNote -> subjectNote)
                .average()
                .orElse(0.00);

        return Math.round(allGradesSum * 100) / 100.00;
    }

    @Override
    public double getAllSubjectsAverageGrade(Map<String, List<Double>> allGrades) {
        int allGradesCount = 0;
        double allGradesSum = 0;

        if (allGrades.isEmpty()) {
            throw new NullPointerException("No saved data in the database!");
        }

        for (String subject : allGrades.keySet()) {
            allGradesSum += this.getAverageGrade(subject);
            allGradesCount++;
        }

        double averageGrade = allGradesSum / allGradesCount;

        return Math.round(averageGrade * 100) / 100.00;
    }
}