package com.novarto.training22.tasks02.schoolNotebook;

import java.util.List;
import java.util.Map;

public interface Notebook {
    /**
     * This method takes the name of a school subject and the grade corresponding to it,
     * then records them. If the subject exist, then the grade can be recorded to it.
     * @param subject should be String object, that describes real school subject
     * @param grade should be floating point double and can't be less than 2 and more than 6
     * @throws IllegalArgumentException if given grade is less than 2 and more than 6
     */
    void saveSchoolGrades(String subject, double grade);

    /**
     * This method takes as a parameter a school subject and
     * based on all grades to it, returns an average grade.
     * @param subject should be String object, that describes real school subject
     * @return average grade for given school subject
     * @throws IllegalArgumentException if the given subject doesn't exist or no records are saved
     */
    double getAverageGrade(String subject);

    /**
     * This method takes all grades for all school subjects and based on its average grades,
     * calculate an average grade for all school subjects.
     * @param allGrades is a Map, that contains all subjects and corresponding grades to any of them.
     * @return average grade for all subjects
     * @throws NullPointerException if no records exists in the map
     */
    double getAllSubjectsAverageGrade(Map<String, List<Double>> allGrades);
}
