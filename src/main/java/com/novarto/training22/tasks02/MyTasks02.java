package com.novarto.training22.tasks02;

import java.util.List;

public class MyTasks02 {
    //Task01a
    public int countNextBiggerNumber(List<Integer> numbers) {
        int count = 0;

        if (numbers.size() == 1) {
            return 0;
        }

        for (int i = 1; i < numbers.size(); i++) {
            if (numbers.get(i) > numbers.get(i - 1)) {
                count++;
            }
        }
        return count;
    }

    //Task01b
    public int countBiggerSums(List<Integer> numbers) {
        int count = 0;
        int sum = 0;

        if (numbers.size() >= 3) {
            sum = numbers.get(numbers.size() - 1) + numbers.get(numbers.size() - 2)
                    + numbers.get(numbers.size() - 3);
        } else if (numbers.size() == 2) {
            return 1;

        } else {
            throw new IllegalArgumentException("No comparison possible with one or less than one number.");
        }

        for (int i = numbers.size() - 2; i >= 0; i--) {
            int currentSum = 0;

            if (i > 1) {
                currentSum = numbers.get(i) + numbers.get(i - 1) + numbers.get(i - 2);
            } else if (i == 1) {
                currentSum = numbers.get(1) + numbers.get(0);
            } else {
                currentSum = numbers.get(i);
            }

            if (sum > currentSum) {
                count++;
            }

            sum = currentSum;
        }

        return count;
    }

    //Task02
    public int findSubmarine(String[] coordinates) {
        int forward = 0;
        int depth = 0;

        for (String coordinate : coordinates) {
            String[] data = coordinate.split(" ");

            if (data.length != 2 || Integer.parseInt(data[1]) < 0) {
                throw new IllegalArgumentException("Invalid coordinates!");
            }

            String command = data[0];
            int num = Integer.parseInt(data[1]);

            switch (command) {
                case "forward":
                    forward += num;
                    break;
                case "down":
                    depth += num;
                    break;
                case "up":
                    depth -= num;
                    break;
                default:
                    throw new IllegalArgumentException("Invalid command! The right commands are: up, down, forward");
            }
        }

        return depth < 0 ? 0 : forward * depth;
    }
}
